---
output: 
  pdf_document: 
    latex_engine: xelatex
lang: ru
mainfont: Liberation Serif
sansfont: Liberation Sans
monofont: Liberation Mono
---
```{r, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, warning = FALSE)
```

# Операторы
## Арифметические операторы

### Задание 1
Посчитайте сумму 1, 7 и 5.

Решение:
```{r 01-operators-1}
1 + 7 + 5
```

### Задание 2
Найдите произведение 7 и 13.

Решение:
```{r 01-operators-2}
7 * 13
```

### Задание 3
Возведите 2 в третью степень.

Решение:
```{r 01-operators-3}
2 ** 3
2 ^ 3
```


### Задание 4.
Расставьте скобки исходя из порядка вычисления и вычислите 2 ^ 3 * 2 / 4. Сравните с результатом, если не расставлять скобки.

Решение. 
```{r 01-operators-4}
2 ^ 3 * 2 / 4
((2 ^ 3) * 2) / 4
```


### Задание 5
Найдите логарифм 16 по основанию 2.

Решение:
```{r 01-operators-5}
log(16, base = 2)
```

### Задание 6
Найдите длину окружности, с учетом того, что диаметр `d` равен 6.
```{r 01-operators-6}
pi * 6
```

### Задание 7
Найдите остаток от деления 17 на 3.

Решение:
```{r 01-operators-7}
17 %% 3
```


### Задание 8
Найдите неполное частное при делении 17 на 3.

Решение:
```{r 01-operators-8}
17 %/% 3
```

### Задание 9
Найдите модуль разницы 7 и 9.

Решение:
```{r 01-operators-9}
abs(7 - 9)
```


### Задание 10
Найдите количество размещений из 6 по 2 ($C_6^2$).

Решение:
```{r 01-operators-10}
choose(6, 2)
```


## Оператор присвоения

### Задание 1
Создайте объект `x` со значением 5. Выведите его на печать (`print(x)`).

Решение:
```{r 01-operators-11}
x <- 5
print(x)
```

### Задание 2
Создайте объект `y`, который равен `3 * x`. Выведите его на печать.

Решение:
```{r 01-operators-12}
y <- 3 * x
print(y)
```

### Задание 3
С помощью функции `assign()` создайте объект `z` со значением 99. Выведите его на печать.

Решение:
```{r 01-operators-13}
assign('z', 99)
print(z)
```

### Задание 4
Выведите на печать результат операции `x + y - z`, без создания нового объекта.

Решение:
```{r 01-operators-14}
x + y - z
```


## Логические операторы

###  Задание 1
Сравните 7 и 99, больше ли первое число, чем второе?

Решение:
```{r 01-operators-15}
7 > 99
```

### Задание 2
Сравните, равно ли значение объекта `x` числу 5?

Решение:
```{r 01-operators-16}
x == 5
```

### Задание 3
Проверьте, что значение выражения z - y не равно 84.

Решение:
```{r 01-operators-17}
z - y != 84
```

### Задание 4
Запишите в объект `alarm` результат сравнения, больше ли `x` чем `y`. Выведите объект на печать.

Решение:
```{r 01-operators-18}
alarm <- x > y
print(alarm)
```

### Задание 5
Проверьте, что объект `alarm` имеет значение `TRUE`.

Решение. Несмотря на очевидность решения через прямое сравнение с помощью оператора `==`, лучше воспользоваться отдельной функцией, `isTRUE()` - во-первых, в этой функции есть проверка на длину объекта (она должна быть равна 1), во-вторых, это несколько более прозрачное с точки зрения читабельности и возможных ошибок решение.
```{r 01-operators-19}
alarm == TRUE
isTRUE(alarm)
```


### Задание 6
Выведите результат объединения двух логических сравнений x > y & y < z. Сначала сделайте сравнение каждой пары, а потом - объединение результатов.

Решение:
```{r 01-operators-20}
x > y
y < z
x > y & y < z
```

